const readlineModule = require('readline')

const rl = readlineModule.createInterface({
  input: process.stdin,
  output: process.stdout
});

var players = [{name: undefined, 
				score: 0,
				symbol: "X"},
				{name: undefined,
				score: 0,
				symbol: "O"}]

var board = [	["1","2","3"],
				["4","5","6"],
				["7","8","9"]]

var currentPlayer = 0;
const numPlayers = 2;
const gamesToWin = 3;

function printBoard() {
	console.log(board[0][0]+"|"+board[0][1]+"|"+board[0][2]);
	console.log("-+-+-");
	console.log(board[1][0]+"|"+board[1][1]+"|"+board[1][2]);
	console.log("-+-+-");
	console.log(board[2][0]+"|"+board[2][1]+"|"+board[2][2]);
}

function checkBoard() {
	if ((board[0][0] == board[0][1] && board[0][1] == board[0][2]) || // Horizontals
		(board[1][0] == board[1][1] && board[1][1] == board[1][2]) ||
		(board[2][0] == board[2][1] && board[2][1] == board[2][2]) ||
		(board[0][0] == board[1][1] && board[1][1] == board[2][2]) || // Diagonals
		(board[0][2] == board[1][1] && board[1][1] == board[2][0]) ||
		(board[0][0] == board[1][0] && board[1][0] == board[2][0]) || // Verticals
		(board[0][1] == board[1][1] && board[1][1] == board[2][1]) ||
		(board[0][2] == board[1][2] && board[1][2] == board[2][2])){
			console.log("The game has been won by "+players[currentPlayer].name);
			players[currentPlayer].score += 1;
			if (players[currentPlayer].score > gamesToWin) {
				console.log(players[currentPlayer].name+" has won the best of "+(gamesToWin*2-1));
				board = [["1","2","3"],["4","5","6"],["7","8","9"]];
			} 
	} else if (!(board[0][0]=="1"||board[0][1]=="2"||board[0][2]=="3"
			||board[1][0]=="4"||board[1][1]=="5"||board[1][2]=="6"
			||board[2][0]=="7"||board[2][1]=="8"||board[2][2]=="9")) {
		console.log("The game is a stalemate, resetting the board");
		board = [["1","2","3"],["4","5","6"],["7","8","9"]];
		printBoard();
	}
	//This should not be in here
	currentPlayer = (currentPlayer + 1) % numPlayers;
	console.log(players[currentPlayer].name+" please select the space you wish to play in:");
}

function processTurn (tile) {
	var x = Math.floor((tile-1)/3);
	var y = (tile-1) % 3;
	//console.log(x + " " + y)
	if (board[x][y] == players[0].symbol || board[x][y] == players[1].symbol){
		console.log("That space has already been chosen, please choose another space.");
		console.log("Enter the number for the space you wish to play in " + players[currentPlayer].name);
	} else {
		board[x][y] = players[currentPlayer].symbol;
		printBoard();
		checkBoard();
	}
}
				
console.log("What is the first players name?")

rl.on('line', (input) => {
	//console.log(`Received: ${input}`);
	if (input == "exit") {
		console.log("Exiting game");
		rl.close();
	} else if (players[0].name == null) {
		players[0].name = input;
		console.log("The first players name is now " + players[0].name);
		console.log("What is the second players name?");
	} else if (players[1].name == null) {
		players[1].name = input;
		console.log("The second players name is now " + players[1].name);
		console.log(players[0].name+" please select the space you wish to play in:");
		printBoard();
	} else {
	processTurn(input);
	}
});
