const readlineModule = require('readline')

const rl = readlineModule.createInterface({
  input: process.stdin,
  output: process.stdout
});

var players = [];

var removed = {name:"FaceDown", hand:[]};

var gameStarted = 0;

const cards = [	{name:"Guard", number:1, count:5, command:playGuard, help:"Name a non-Guard card and choose another player.\nIf that player has theat card, he or she is out of the round.\nUse \"play Guard [target] [card choice]\" to play this card."}, 
				{name:"Priest", number:2, count:2, command:playPriest, help:"Look at another players hand.\nUse \"play Priest [target]\" to play this card."}, 
				{name:"Baron", number:3, count:2, command:playBaron, help:"You and another player secretly compare hands.\nThe player with the lower value is out of the round.\nUse \"play Baron [target]\" to play this card."},
				{name:"Handmaid", number:4, count:2, command:playHandmaid, help:"Until your next turn, ignore all effects from other players cards.\n Use \"play Handmaid\" to play this card."},
				{name:"Prince", number:5, count:2, command:playPrince, help:"Choose any player (including yourself) to discard his or her hand and draw a card.\n(If the deck is empty he or she draws the card removed from the deck at the start of the game.)\nUse \"play Prince [target]\" to play this card."},
				{name:"King", number:6, count:1, command:playKing, help:"Trade hands with another player of your choice.\nUse \"play King [target]\" to play this card."},
				{name:"Countess", number:7, count:1, command:playCountess, help:"If you have this card and the King or Prince in your hand, you must discard this card.\nUse \"play Countess\" to play this card."},
				{name:"Princess", number:8, count:1, command:playPrincess, help:"If you discard this card you are out of the round.\nUse \"play Princess\" to play this card."}];

var deck = [];
var discarded = [];

var activePlayers = 0;

var currentPlayer = 0;

var gameWon = 0;

function getPlayerFromName (name) {
	for (let i = 0; i < players.length; i++){
		if (players[i].name == name) {
			return i;
		}
	}
	sendMassMessage("Error, "+name+" is not a known player in this game.")
}

function playGuard (target_guess) {
	var parts = target_guess.split(" ");
	var target = getPlayerFromName(parts[0]);
	if (players[target].protection) {
		sendMassMessage(players[currentPlayer].name+" played a Guard against "+players[target].name+", but "+players[target].name+" is protected by a Handmaid.");
	} else {
		var guess = parts[1];
		sendMassMessage(players[currentPlayer].name+" played a Guard and is guessing that "+players[target].name+" holds a "+guess+".");
		if (cards[players[target].hand[0]-1].name == guess) {
			sendMassMessage("They were correct, "+players[target].name+" is out of the round.")
			killPlayer(players[target]);
		} else {
			//console.log(cards[players[target].hand[0]-1].name);
			//console.log(guess);
			sendMassMessage("They were incorrect.")
		}
	}
}

function playPriest (targetName) {
	var target = getPlayerFromName(targetName);
	if (players[target].protection) {
		sendMassMessage(players[currentPlayer].name+" played a Priest against "+players[target].name+", but "+players[target].name+" is protected by a Handmaid.");
	} else {
		sendMassMessage(players[currentPlayer].name+" played a Priest and is looking at "+players[target].name+"s hand.");
		sendMessage(players[currentPlayer], players[target].name+" has a "+cards[players[target].hand[0]-1].name+" in their hand.");
	}
}

function playBaron (targetName) {
	var target = getPlayerFromName(targetName);
	if (players[target].protection) {
		sendMassMessage(players[currentPlayer].name+" played a Baron against "+players[target].name+", but "+players[target].name+" is protected by a Handmaid.");
	} else {
		sendMassMessage(players[currentPlayer].name+" played a Baron and is comparing hands with "+players[target].name+".");
		if (players[target].hand[0] > players[currentPlayer].hand[0]) {
			sendMassMessage(players[target].name+" has the higher value card. "+players[currentPlayer].name+" is out of the round.")
			killPlayer(players[currentPlayer])
		} else if (players[currentPlayer].hand[0] > players[target].hand[0]) {
			sendMassMessage(players[currentPlayer].name+" has the higher value card. "+players[target].name+" is out of the round.")
			killPlayer(players[target]);
		} else {
			sendMassMessage(players[currentPlayer].name+" and "+players[target].name+" have the same card in their hands! Nothing happens.")
		}
	}
}

function playHandmaid () {
	sendMassMessage(players[currentPlayer].name+" played a Handmaid and is protected until the start of their next turn.");
	players[currentPlayer].protection = 1;
}

function playPrince (targetName) {
	var target = getPlayerFromName(targetName);
	if (players[target].protection) {
		sendMassMessage(players[currentPlayer].name+" played a Prince against "+players[target].name+", but "+players[target].name+" is protected by a Handmaid.");
	} else {
		sendMassMessage(players[currentPlayer].name+" played a Prince and is forcing "+players[target].name+" to discard their hand.");
		var discardedCard = players[target].hand.splice(0,1)
		discardCard(players[target], discardedCard);
		if (discardedCard == 8){
			sendMassMessage(players[target].name+" discarded the Princess and is out of the round.");
			killPlayer(players[target]);
		} else {
			if (deck.length > 0) {
				dealCard(players[target]);
			} else {
				sendMassMessage("As the deck is empty "+players[target].name+" draws the card that was placed face down.")
				players[target].hand.push(removed.hand[0]);
			}
		}
	}
}

function playKing (targetName) {
	var target = getPlayerFromName(targetName);
	if (players[target].protection) {
		sendMassMessage(players[currentPlayer].name+" played a King against "+players[target].name+", but "+players[target].name+" is protected by a Handmaid.");
	} else {
		sendMassMessage(players[currentPlayer].name+" played a King and is forcing "+players[target].name+" to swap their hand.");
		var targetCard = players[target].hand.splice(0,1);
		var playerCard = players[currentPlayer].hand.splice(0,1);
		players[target].hand.push(playerCard);
		players[currentPlayer].hand.push(targetCard)
		sendMessage(players[target],"Your "+cards[targetCard-1].name+" card has been replaced with a "+cards[playerCard-1].name+".")
		sendMessage(players[currentPlayer],"Your "+cards[playerCard-1].name+" card has been replaced with a "+cards[targetCard-1].name+".")
	}
}

function playCountess () {}

function playPrincess () {
	sendMassMessage(players[currentPlayer].name+" played the Princess and is out of the round.");
	killPlayer(players[currentPlayer])
}

function killPlayer (player) {
	player.active = 0;
	for (let i=player.hand.length; i>0; i--){
		discardCard(player, player.hand.pop());
	}
	activePlayers -= 1;
}

function shuffle(a) {
    var j, x, i;
    for (i = a.length - 1; i > 0; i--) {
        j = Math.floor(Math.random() * (i + 1));
        x = a[i];
        a[i] = a[j];
        a[j] = x;
    }
}

function joinGame (playerName) {
	console.log("Adding "+playerName+" to the list of players");
	players.push({name:playerName, score:0, hand:[], active:1, protection:0});
}

function sendMessage (recipient, message) {
	console.log("Message for "+recipient.name+" : "+message);
}

function sendMassMessage (message) {
	for (let i = 0; i < players.length; i++) {
		sendMessage(players[i], message)
	}
}

function dealCard (recipient) {
	sendMassMessage("Dealing a card to "+recipient.name);
	recipient.hand.push(deck.pop());
	sendMessage(recipient, "You have been dealt the card: "+recipient.hand[recipient.hand.length-1])
}

function discardCard (source, card) {
	sendMassMessage(source.name+" has discarded a "+cards[card-1].name+" card.")
	discarded.push(card);
}

function checkWinner() {
	for (let i = 0; i < players.length; i++) {
		if (players[i].score >= 4){
			sendMassMessage(players[i].name+" has won the game with a score of "+players[i].score);
			gameWon = 1;
			rl.close();
		}
	}
}

function resetGame () {
	deck = [];
	for (let i=0; i<cards.length; i++){
		for (let j=0; j<cards[i].count; j++){
			deck.push(cards[i].number);
		}
	}
	shuffle(deck);
	removed.hand.pop();
	dealCard(removed);
	for (let i = 0; i < players.length; i++) {
		while (players[i].hand.length > 0) {
			players[i].hand.pop();
		}
		players[i].active=1;
		dealCard(players[i]);
	}
	activePlayers = players.length;
	dealCard(players[currentPlayer]);
	sendMessage(players[currentPlayer], "It is your turn, your hand contains the cards "+players[currentPlayer].hand[0]+" and "+players[currentPlayer].hand[1]+".");
	sendMessage(players[currentPlayer], "To play your card type \"play X\" where X is the name or number of the card you wish to play.")
}

function playCard (player, inputStr) {
	
	if (players[currentPlayer].protection) {
		players[currentPlayer].protection = 0;
	}
	
	var parts = inputStr.split(" ");
	var cardNum = Number(parts[0]);
	var card = cards[cardNum-1]
	//Remove the card number from the input string
	parts.splice(0,1);
	
	if (cardNum != 7 && players[currentPlayer].hand.indexOf(7) >= 0 && (players[currentPlayer].hand.indexOf(5) >= 0 || players[currentPlayer].hand.indexOf(6) >= 0)) {
		sendMessage(players[currentPlayer], "You must play the Countess this turn. Enter \"play 7\" to continue.");
		return;
	}
	
	//Let everyone know what was played
	sendMassMessage(players[player].name+ " has played the card "+card.name);
	
	//Remove the card from the players hand
	var cardLoc = players[player].hand.indexOf(cardNum)
	
	//console.log(players[player].hand.join(" "))
	//console.log(cardLoc);
	//console.log(cardNum);
	
	discardCard(players[player],players[player].hand.splice(cardLoc, 1));
	
	//Calls the effect of the card and supplies all input give by the user
	card.command(parts.join(" "));
	
	//Move play to the next player
	currentPlayer = (currentPlayer + 1) % players.length;
	
	while (!players[currentPlayer].active) {
		//Keep cycling till we find the next active player
		currentPlayer = (currentPlayer + 1) % players.length;
	}
	if (activePlayers == 1 || deck.length <= 0 ) {
		//If the deck is empty or there is only one player left the round is over
		var bestIndex = 0;
		if (activePlayers == 1){
			sendMassMessage("Only one player remains, the round is now over.");
			bestIndex = currentPlayer;
		} else {
			sendMassMessage("The deck is empty, the round is now over.");
			for (let i = 1; i < players.length; i++) {
				if (players[i].hand[0] > players[bestIndex].hand[0]) {
					bestIndex = i;
				}
			}
		}
		sendMassMessage("The winner of the round is "+players[bestIndex].name+" who has the card " + players[bestIndex].hand[0]);
		sendMassMessage("The face down card was "+removed.hand[0]);
		players[bestIndex].score += 1;
		checkWinner();
		if (!gameWon){
			resetGame();
		}
	} else {
		sendMassMessage("It is now "+players[currentPlayer].name+"s turn");
		dealCard(players[currentPlayer]);
		if (players[currentPlayer].hand.indexOf(7) >= 0 && (players[currentPlayer].hand.indexOf(5) >= 0 || players[currentPlayer].hand.indexOf(6) >= 0)) {
			sendMessage(players[currentPlayer], "It is your turn, your hand contains the cards "+players[currentPlayer].hand[0]+" and "+players[currentPlayer].hand[1]+".");
			sendMessage(players[currentPlayer], "You must play the Countess. Enter \"play 7\" to play this card.");
		} else {
			sendMessage(players[currentPlayer], "It is your turn, your hand contains the cards "+players[currentPlayer].hand[0]+" and "+players[currentPlayer].hand[1]+".");
			sendMessage(players[currentPlayer], "To play your card type \"play X\" where X is the name or number of the card you wish to play.");
		}
	}
}

function showHelp (requester, subject){
	for (let i=0; i<cards.ength; i++) {
		if (cards[i].name == subject){
			return sendMessage(requester, cards[i].help);
		}
	}
	if (subject == "about") {
		return sendMessage(requester, "Love letter js. Written by a hubling for hublings.");
	}
	if (subject == "how to play") {
		return sendMessage(requester, "Each player has a hand of one card. On your turn you will draw another card and choose which of the two you wish to play.\nUse \"play X [target] [card option]\" to play your chosen card, specifying a target if one is needed by the card, and other options as required by the card.\nUse \"help [card name]\" or \"help [card number]\" to get the ability and usage of a specific card. The objective of the game is to win four rounds by being the last active player left.");
	}
	sendMessage(requester, "Welcome to Love Letter js.\nUse \"help about\" to learn more about this game, or \"help how to play\" to get more information about how to play this game.\nYou can also use \"help [card name]\" or \"help [card number]\" to get help about a specific card.");
}

rl.on('line', (input) => {
	if (input == "exit") {
		//Exits the game
		console.log("The players were:")
		for (let i = 0; i < players.length; i++) {
			console.log(players[i].name+" "+players[i].score)
		}
		console.log("Exiting game");
		rl.close();
	} else if (input.startsWith("join ") && !gameStarted) {
		//Adds another player to the game
		name = input.split(" ");
		name.splice(0,1);
		joinGame(name.join(" "));
	} else if (input == "start game" && players.length > 0) {
		//Start the game
		gameStarted = 1;
		currentPlayer = Math.floor(Math.random()*players.length)
		resetGame();
	} else if (input.includes(" play ") && gameStarted) {
		//plays a card from the current player
		console.log(input);
		choice = input.split(" ");
		choice.splice(1,1);
		var transmitPlayer = choice[0];
		choice.splice(0,1);
		if (players[currentPlayer].name == transmitPlayer) {
			playCard(currentPlayer, choice.join(" "));
		} else {
			for (let i = 0; i < players.length; i++) {
				if (players[i].name == transmitPlayer) {
					sendMessage(players[i], "It is not your turn, please wait until instructed to submit your move.")
				}
			}
		}
	} else if (input.includes(" help ")) {
		console.log(input);
                choice = input.split(" ");
                choice.splice(1,1);
                var transmitPlayer = getPlayerFromName(choice[0]);
                choice.splice(0,1);
		showHelp(transmitPlayer, choice.join(" "));
	} else {
		//Input we don't know what to do with at the moment
		console.log("Got garbage:");
		console.log(input);
		console.log(input);
                choice = input.split(" ");
                choice.splice(1,1);
                var transmitPlayer = getPlayerFromName(choice[0]);
		showHelp(transmitPlayer, "");
	}
});
console.log("use \"join X\" to add the player named X to the game. Use \"start game\" to start the game.")
