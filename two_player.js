const readlineModule = require('readline')

const rl = readlineModule.createInterface({
  input: process.stdin,
  output: process.stdout
});

var players = [{name: undefined, 
				score: 0,
				chosen: 0},
				{name: undefined,
				score: 0,
				chosen: 0}
				]

const winPoints = 2;
const losePoints = 1;
const backstabWinPoints = 10;
const backstabLosePoints = 5;

function processRound () {
	if (players[0].chosen == 1 && players[1].chosen == 8) {
		console.log(players[1].name + " was backstabbed by "+players[0].name+"!");
		players[0].score += backstabWinPoints;
		players[1].score -= backstabLosePoints;
	} else if (players[0].chosen == 8 && players[1].chosen == 1) {
		console.log(players[0].name + " was backstabbed by "+players[1].name+"!");
		players[1].score += backstabWinPoints;
		players[0].score -= backstabLosePoints;
	} else if (players[0].chosen > players[1].chosen){
		console.log(players[0].name+" chose a higher card than "+players[1].name+"!");
		console.log(players[0].chosen+" beats "+players[1].chosen);
		players[0].score += winPoints;
		players[1].score -= losePoints;
	} else if (players[0].chosen == players[1].chosen) {
		console.log("You both picked the same card! Try again!");
	} else {
		console.log(players[1].name+" chose a higher card than "+players[0].name+"!");
		console.log(players[1].chosen+" beats "+players[0].chosen);
		players[1].score += winPoints;
		players[0].score -= losePoints;
	}
	if (players[0].score >= 20) {
		console.log(players[0].name + " has beaten " + players[1].name + " with a score of " + players[0].score + " to " + players[1].score);
		rl.close()
	} else if (players[1].score >= 20) {
		console.log(players[1].name + " has beaten " + players[0].name + " with a score of " + players[1].score + " to " + players[0].score);
		rl.close();
	} else {
		console.log("The scores are now:");
		console.log(players[0].name+"\t:"+players[0].score);
		console.log(players[1].name+"\t:"+players[1].score);
		console.log(players[0].name+" please play a card from 1-8:");
		players[0].chosen = 0;
		players[1].chosen = 0;
	}
}

console.log("What is the first players name?")

rl.on('line', (input) => {
	//console.log(`Received: ${input}`);
	if (players[0].name == null) {
		players[0].name = input;
		console.log("The first players name is now " + players[0].name)
		console.log("What is the second players name?")
	} else if (players[1].name == null) {
		players[1].name = input;
		console.log("The second players name is now " + players[1].name)
		console.log(players[0].name+" please play a card from 1-8:")
	} else if (players[0].chosen == 0) {
		players[0].chosen = input;
		console.log(players[1].name+" please play a card from 1-8:")
	} else if (players[1].chosen == 0) {
		players[1].chosen = input;
		processRound()
	}
});