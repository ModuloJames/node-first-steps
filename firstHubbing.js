var nmdc = require('nmdc');
var net = require('net');

const HOST = '127.0.0.1';
const PORT = 6969;

var client = new net.Socket();
client.connect(PORT, HOST, function() {
	console.log('Connected to: '+HOST+':'+PORT);
});

client.on('data', function(data) {
	console.log('DATA: '+data);
	var parts = String(data).split(" ");
	var username = parts[0];
	parts.splice(0,1);
	hub.pm(username, parts.join(" "));
});

client.on('close', function() {
	console.log('Connection closed');
});

var hub = new nmdc.Nmdc({
	nick: 'ScrubLord',
	auto_reconnect: true,
	address: '127.0.0.1'
});

hub.onPrivate = function (username, message) {
	hub.say(username+" just told me "+message);
	client.write(username+" "+message);
}
