const readlineModule = require('readline')

const rl = readlineModule.createInterface({
  input: process.stdin,
  output: process.stdout
});

const defaultCardDists = [5,2,2,2,2,1,1,1]

var player = {	name: undefined, 
				score: 0
};

const winPoints = 2;
const losePoints = 1;
const backstabWinPoints = 10;
const backstabLosePoints = 5;

function processInput (chosenCard) {
	var opponent = 1 + Math.floor(8*Math.random());
	if (chosenCard == 1 && opponent == 8) {
		console.log("You backstabbed your opponent!");
		player.score += backstabWinPoints;
	} else if (chosenCard == 8 && opponent == 1) {
		console.log("You were backstabbed!");
		player.score -= backstabLosePoints;
	} else if (chosenCard > opponent){
		console.log("You won: "+chosenCard+":"+opponent);
		console.log("Gain a point!");
		player.score += winPoints;
	} else if (chosenCard == opponent) {
		console.log("You picked the same card! Try again!");
	} else {
		console.log("You lost: "+opponent+":"+chosenCard);
		player.score -= losePoints;
	}
	if (player.score >= 20) {
		console.log("Congratulations! You have won the game with a score of "+player.score+"!")
		rl.close()
	} else if (player.score <= -20) {
		console.log("You have lost with a score of "+player.score+".");
		console.log("Better luck next time.");
		rl.close();
	} else {
		console.log("You now have "+player.score+" points.");
		console.log("Play a card from 1-8:");
	}
}

console.log("What is your name?")

rl.on('line', (input) => {
	//console.log(`Received: ${input}`);
	if (player.name == null) {
		player.name = input;
		console.log("The players name is now " + player.name)
		console.log("Play a card from 1-8:")
	} else {
		processInput(input)
	}
});